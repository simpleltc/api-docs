MDS 3.0 Batches and Assessments
***************************************

The MDS Transmission API is designed to allow simplified access to the SimpleLTC MDS transmission workflow.

SimpleLTC receives batches from providers, transmits them to QIES / CMS, and then automatically retrieves the
corresponding final validation reports from the CASPER system. The API allows third-party software to participate
in the transmission workflow by acting on behalf of the provider.

.. image:: images/batch_workflow.png
   :width: 630px
   :height: 461px

Default Parameters
======================================
Each resultset for MDS Batch and Assessment search calls are automatically
paginated with a default page size of 100 results. It is not currently
possible to query for a larger or smaller number of results.

Uploading MDS Batches
======================================

.. http:post:: /MdsBatches

   Uploads an MDS batch file for transmission to QIES / CMS.
   Batch data can be posted raw as application/octet-stream or base64-encoded as application/base64.

   :query filename: The filename of the file being uploaded (required)
   :type filename: string
   :status 201: The MDS batch was accepted.
   :status 400: Request was not acceptable. Check the response for details.

   **Error Codes**

   * **1**: The batch is a duplicate and cannot be accepted for upload.
   * **2**: Your account is not authorized to upload for this provider.
     Either the account does not have access or the provider's service was
     terminated or suspended.
   * **3**: The batch format is invalid and could not be accepted. Verify the
     batch is acceptable under the MDS 3.0 Submission Specifications.

   **Example Request**

   .. sourcecode:: http

      POST /MdsBatches?filename=123XM18.zip HTTP/*1.1
      Accept: application/json
      Content-Type: application/octet-stream
      Content-Length: 58616
      {batch file data}

   **Example Response**

   .. sourcecode:: http

        HTTP/1.1 201 Created
        Cache-Control: no-cache
        Pragma: no-cache
        Content-Type: application/json; charset=utf-8
        Expires: -1
        Date: Thu, 18 Oct 2012 19:00:42 GMT
        Content-Length: 264

        {
           "id": 1826984,
           "provider": {
                    "id":4002,
                    "name":"Orange Nursing",
                    "state":"TX",
                    "npi":"1111111112",
                    "ccn":"9987175",
                    "mdsFacilityId":"TX1002"
           },
           "filename": "123XM16.zip",
           "status": "Waiting for Transmit",
           "webStatus": "Pending Transmission",
           "statusDate": "2012-10-18T14:00:38.54Z",
           "uploadDate": "2012-10-18T14:00:38.54Z",
           "totalRecords": 21
        }

   **Example Error**

   .. sourcecode:: http

        HTTP/1.1 400 Bad Request
        Cache-Control: no-cache
        Pragma: no-cache
        Content-Type: application/json; charset=utf-8
        Expires: -1
        Date: Thu, 18 Oct 2012 19:00:42 GMT
        Content-Length: 79

        {
            "ErrorCode":3,
            "Message":"Unrecognized signature for batch: batch.zip: [12]"
        }


Searching Batches
======================================
.. http:get:: /MdsBatches/

	Retrieves a listing of MDS batches ordered by their upload date.

	:query provider_id: Filter to include only batches specified by the provider's unique ID.
	:type provider_id: string
	:query status_updated_after: Filter to include only batches that have a status date after the specified date
	:type status_updated_after: datetime
        :query status_updated_before: Filter to include only batches that have a status date prior to the specified date
	:type status_updated_before: datetime
        :query status: Filter to include only batches with the specified status
        :type status: string
	:query uploaded_after: Filter to only include batches uploaded after the specified date/time
	:type uploaded_after: datetime
	:query uploaded_before: Filter to only include batches uploaded before the specified date/time
	:type uploaded_before: datetime
        :query page: Indicates which page of the resultset to return (default is 1)
        :type page: int

	**Example Request**

	.. sourcecode:: http

		GET /MdsBatches HTTP/1.1
		Accept: application/json

	**Example Response**

	.. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Expires: -1
		Date: Thu, 25 Oct 2012 15:47:54 GMT
		Content-Length: 8575

		[
			{
				"id": 529234,
				"provider": {
                                        "id":4002,
                                        "name":"Orange Nursing",
                                        "state":"TX",
                                        "npi":"1111111112",
                                        "ccn":"9987175",
                                        "mdsFacilityId":"TX1002"
                                },
				"filename": "20110222145414-1.zip",
				"status": "Waiting for Transmit",
                                "webStatus": "Pending Transmission",
				"statusDate": "2011-02-23T15:53:21.967Z",
				"uploadDate": "2012-10-03T15:53:21.967Z",
				"totalRecords": 4,
				"cmsBatchID": "868007",
				"links": null
			},
			{
				"id": 529235,
				"provider": {
                                        "id":4002,
                                        "name":"Orange Nursing",
                                        "state":"TX",
                                        "npi":"1111111112",
                                        "ccn":"9987175",
                                        "mdsFacilityId":"TX1002"
                                },
				"filename": "20110221170005-1.zip",
				"status": "Feedback Received",
                                "webStatus": "Waiting for Final Validation",
				"statusDate": "2011-02-23T14:42:32.39Z",
				"uploadDate": "2012-10-03T14:42:32.39Z",
				"totalRecords": 5,
				"cmsBatchID": "862794",
				"links": null
			}
		]



Checking Batch Status
======================================
.. http:get:: /MdsBatches/{batch_id}

   Retrieves information about a specific batch.

   :param batch_id: The unique identifier for the batch in the SimpleLTC system
   :type batch_id: string
   :status 401: You do not have access to the specified batch.
   :status 404: The specified batch does not exist.

   **Example Request**

   .. sourcecode:: http

      GET /MdsBatches/1826984 HTTP/1.1
      Accept: application/json

   **Example Response**

   .. sourcecode:: http

	   HTTP/1.1 200 OK
	   Cache-Control: no-cache
	   Pragma: no-cache
	   Content-Type: application/json; charset=utf-8
	   Expires: -1
	   Date: Thu, 18 Oct 2012 19:00:42 GMT
	   Content-Length: 264


	   {
		"id": 1826984,
		"provider": {
                        "id":4002,
                        "name":"Orange Nursing",
                        "state":"TX",
                        "npi":"1111111112",
                        "ccn":"9987175",
                        "mdsFacilityId":"TX1002"
                },
		"filename": "123XM16.zip",
		"status": "Final Received",
                "webStatus": "Final Validation Received",
		"statusDate": "2012-10-18T14:00:38.54Z",
		"uploadDate": "2012-10-18T14:00:38.54Z",
		"totalRecords": 21
	   }

Downloading a Batch
======================================
.. http:get:: /MdsBatches/{batch_id}

   Retrieves the raw binary data for a specific batch. This batch will return the binary data of the original file uploaded to SimpleLTC.
   This request is differentiated from other get requests by using the proper MIME type.

   :mimetype: application/octet-stream
   :param batch_id: The unique identifier for the batch in the SimpleLTC system
   :type batch_id: string
   :status 401: You do not have access to the specified batch.
   :status 404: The specified batch does not exist.

   **Example Request**

   .. sourcecode:: http

		GET /MdsBatches/1826984 HTTP/1.1
		Accept: application/octet-stream

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/octet-stream
		Expires: -1
		Date: Mon, 22 Oct 2012 14:03:48 GMT
		Content-Length: 12551

		{binary data of original batch}

Retrieving Final Validation Reports
======================================
.. http:get:: /MdsFinalValidation/{batch_id}

   Retrieves the raw final validation report for a specific batch. Only batches that have a status of
   "Final Received" will return data from this method. All others will return a 404.

   :param batch_id: The unique identifier for the batch in the SimpleLTC system
   :type batch_id: string
   :status 401: You do not have access to the specified batch.
   :status 404: The specified batch does not yet have a final validation or the batch was not found.

   **Example Request**

   .. sourcecode:: http

      GET /MdsFinalValidation/1826984 HTTP/1.1
      Accept: text/plain

   **Example Response**

   .. sourcecode:: http

	   HTTP/1.1 200 OK
	   Cache-Control: no-cache
	   Pragma: no-cache
	   Content-Type: text/plain
	   Expires: -1
	   Date: Thu, 18 Oct 2012 19:00:42 GMT
	   Content-Length: 26423


					                     CMS Submission Report
				            MDS 3.0 NH Final Validation Report

		Submission Date/Time:              06/13/2012 13:48:32
		Processing Completion Date/Time:   06/13/2012 13:57:12
		Submission ID:                     54545454
		Submission File Status:            Completed
		State Code:                        TX
		Facility ID:                       555555
		...



Searching Assessments
======================================
.. http:get:: /MdsAssessments/

   Retrieves a listing of MDS assessments.

   :query provider_id: Filter to include only assessments matching a provider's unique ID.
   :type provider_id: string
   :query batch_id: Filter to include only assessments from a specific batch upload.
   :type batch_id: string
   :query status_updated_after: Filter to include only assessments from batches that have a status date after the specified date.
   :type status_updated_after: datetime
   :query status_updated_before: Filter to include only assessments from batches that have a status date prior to the specified date.
   :type status_updated_before: datetime
   :query status: Filter to include only assessments with the specified status
   :type status: string
   :query uploaded_after: Filter to only include assessmnets uploaded after the specified date/time
   :type uploaded_after: datetime
   :query uploaded_before: Filter to only include assessments uploaded before the specified date/time
   :type uploaded_before: datetime
   :query target_date_after: Filter to only include assessments with a target date after the specified date.
   :type target_date_after: datetime
   :query target_date_before: Filter to only include assessments with a target date before the specified date.
   :type target_date_before: datetime
   :query page: Indicates which page of the resultset to return (default is 1)
   :type page: int

   **Example Request**

   .. sourcecode:: http

		GET /MdsAssessments/ HTTP/1.1
		Accept: application/json

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Expires: -1
		Date: Wed, 24 Oct 2012 20:40:51 GMT
		Content-Length: 1435

		[
			{
				"batchID": 528962,
				"id": 2234535,
				"residentName": "MANUEL, DIEGO",
				"dateOfBirth": "1900-01-01",
				"ssn": "000000000",
				"medicaidID": "0000000000",
				"medicareID": "0000000000",
				"filename": "F5_15123_ND_1252011.xml",
				"assessmentType": "Entry Tracking Record",
				"versionCode": "1.00",
				"correction": 0,
				"targetDate": "2011-01-25",
				"status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT",
				"calculatedFields": {}
			},
			{
				"batchID": 528962,
				"id": 2234536,
				"residentName": "WILSON, JOHN",
				"dateOfBirth": "1900-01-01",
				"ssn": "000000000",
				"medicaidID": "0000000000",
				"medicareID": "0000000000",
				"filename": "F5_15123_NP_232011.xml",
				"assessmentType": "Entry Tracking Record",
				"versionCode": "1.00",
				"correction": 0,
				"targetDate": "2011-02-03",
				"status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT",
				"calculatedFields": {}
			},
			{
				"batchID": 528962,
				"id": 2234537,
				"residentName": "STEVENS, THOMAS",
				"dateOfBirth": "1900-01-01",
				"ssn": "000000000",
				"medicaidID": "0000000000",
				"medicareID": "0000000000",
				"filename": "F5_15123_NT_1272011.xml",
				"assessmentType": "Entry Tracking Record",
				"versionCode": "1.00",
				"correction": 0,
				"targetDate": "2011-01-27",
				"status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT",
				"calculatedFields": {}
			},
			{
				"batchID": 528962,
				"id": 2234538,
				"residentName": "KELLY, SAMUEL",
				"dateOfBirth": "1900-01-01",
				"ssn": "000000000",
				"medicaidID": "0000000000",
				"medicareID": "0000000000",
				"filename": "F5_15636_NC_232011.xml",
				"assessmentType": "Entry Tracking Record",
				"versionCode": "1.00",
				"correction": 0,
				"targetDate": "2011-02-03",
				"status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT",
				"calculatedFields": {}
			}
		]



Checking Assessment Status
======================================
.. http:get:: /MdsAssessments/{assessment_id}

   Retrieves information about a specific MDS assessment

   :param assessment_id: The unique identifier for the assessment in the SimpleLTC system
   :type assessment_id: string
   :status 401: You do not have access to the specified assessment.
   :status 404: The specified assessment does not exist.

   **Example Request**

   .. sourcecode:: http

      GET /MdsAssessments/2235010 HTTP/1.1
      Accept: application/json

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Expires: -1
		Date: Wed, 24 Oct 2012 20:40:51 GMT
		Content-Length: 340

		{
			"batchID": 529251,
			"id": 2235010,
			"residentName": "HAYES, EDITH",
			"dateOfBirth": null,
			"ssn": "000000000",
			"medicaidID": "0000000000",
			"medicareID": "0000000000",
			"filename": "SO_20929_NT_2142011.xml",
			"assessmentType": "Entry Tracking Record",
			"versionCode": "1.00",
			"correction": 0,
			"targetDate": "2011-02-14",
			"status": "Accepted",
            "a0200":"1",
            "a0310a":"99",
            "a0310b":"99",
            "a0310c":"0",
            "a0310d":"^",
            "a0310f":"12",
            "isc":"NT",
			"calculatedFields": {}
		}


Downloading an Assessment XML File
======================================
.. http:get:: /MdsAssessments/{assessment_id}

   Retrieves the raw XML data for a specific MDS assessment.
   This request is differentiated from other get requests by using the proper MIME type.

   :mimetype: application/octet-stream
   :param assessment_id: The unique identifier for the assessment in the SimpleLTC system
   :type assessment_id: string
   :status 401: You do not have access to the specified assessment.
   :status 404: The specified assessment does not exist.

   **Example Request**

   .. sourcecode:: http

		GET /MdsAssessments/2235010 HTTP/1.1
		Accept: application/octet-stream

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/octet-stream
		Expires: -1
		Date: Wed, 24 Oct 2012 20:36:23 GMT
		Content-Length: 2679

		<?xml version="1.0" standalone="yes"?>
		<ASSESSMENT>
		    <ASMT_SYS_CD>MDS</ASMT_SYS_CD>
		    <ITM_SBST_CD>NT</ITM_SBST_CD>
		    <ITM_SET_VRSN_CD>1.00</ITM_SET_VRSN_CD>
		    <SPEC_VRSN_CD>1.00</SPEC_VRSN_CD>
		    <PRODN_TEST_CD>P</PRODN_TEST_CD>
		    <STATE_CD>TX</STATE_CD>
		    <FAC_ID>43432</FAC_ID>
		    <SFTWR_VNDR_ID>21232353432</SFTWR_VNDR_ID>
		    <SFTWR_VNDR_NAME>AMERICAN HEALTHTECH, INC.</SFTWR_VNDR_NAME>
		    <SFTWR_VNDR_EMAIL_ADR>bcribb@healthtech.net</SFTWR_VNDR_EMAIL_ADR>
		    <SFTWR_PROD_NAME>LTC</SFTWR_PROD_NAME>
		    <SFTWR_PROD_VRSN_CD>8.7.2.500</SFTWR_PROD_VRSN_CD>

Requesting MDS Assessments Asynchronusly
======================================
.. http:post:: /MdsAssessmentsAsync/

   Submits an asynchronus request to generate an archive of MDS assessments. At least, one pair of date parameters is required.

   :query provider_id: Required. Filter to include only assessments matching a provider's unique ID.
   :type provider_id: int
   :query uploaded_after: Filter to only include assessmnets uploaded after the specified date/time
   :type uploaded_after: datetime
   :query uploaded_before: Filter to only include assessments uploaded before the specified date/time
   :type uploaded_before: datetime
   :query target_date_after: Filter to only include assessments with a target date after the specified date.
   :type target_date_after: datetime
   :query target_date_before: Filter to only include assessments with a target date before the specified date.
   :type target_date_before: datetime
   :query status_updated_after: Filter to include only assessments from batches that have a status date after the specified date.
   :type status_updated_after: datetime
   :query status_updated_before: Filter to include only assessments from batches that have a status date prior to the specified date.
   :type status_updated_before: datetime
   :query status: Filter to include only assessments with the specified status
   :type status: string

   **Example Request**

   .. sourcecode:: http

		POST /MdsAssessmentsAsync HTTP/1.1
		Content-Length: 240
		Content-Type: application/json

		{
			"provider_id":477,
			"target_date_after":"2010-10-01",
			"target_date_before":"2011-10-01"
		}

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 202 Accepted
		Location: /MdsAssessmentsAsync/51ddc07773c044275483b67c
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Expires: -1
		Date: Wed, 24 Oct 2012 20:40:51 GMT
		Content-Length: 340

		{
			"requestId": "51ddc07773c044275483b67c",
			"status": "Processing"
		}


Monitoring MDS Assessments Asynchronus Request Status
======================================
.. http:get:: /MdsAssessmentsAsync/{request_id}

   Checks the status of a previously submitted asynchronus request for MDS assessments.

   The resulting request can have one of three statuses: "Processing", "Complete", "Empty", or "Error".

   :mimetype: application/json
   :param request_id: The unique identifier for the asynchronus request in the SimpleLTC system
   :type request_id: string
   :status 404: The specified request does not exist.

   **Example Request**

   .. sourcecode:: http

		GET /MdsAssessmentsAsync/51ddc07773c044275483b67c HTTP/1.1
		Accept: application/json

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Expires: -1
		Date: Wed, 24 Oct 2012 20:40:51 GMT
		Content-Length: 340

		{
			"requestId": "51ddc07773c044275483b67c",
			"status": "Processing"
		}

Retrieving MDS Assessments Asynchronus Request Results
======================================
.. http:get:: /MdsAssessmentsAsync/{request_id}

   Retrieves the results of a completed asynchronus request for MDS assessments.
   The result will be returned in binary as a ZIP archive.

   :mimetype: application/octet-stream
   :param request_id: The unique identifier for the asynchronus request in the SimpleLTC system
   :type request_id: string
   :status 404: The specified request does not exist or has not finished processing.

   **Example Request**

   .. sourcecode:: http

		GET /MdsAssessmentsAsync/51ddc07773c044275483b67c HTTP/1.1
		Accept: application/octet-stream

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/octet-stream
		Expires: -1
		Date: Wed, 24 Oct 2012 20:40:51 GMT
		Content-Length: 5782

		{ZIP file binary data}

Possible Status Values
======================================

MDS Batch
--------------------------------------
Possible values for the "status" attribute:

* Waiting for Analysis
* Waiting for Approval
* Rejected by User
* Waiting for Transmit
* Transmitting
* Feedback Received
* Final Received
* On Hold

MDS Assessment
--------------------------------------
Possible values for the "status" attribute:

* Accepted
* Rejected
* Waiting

MDS Assessment Async Request
--------------------------------------
Possible values for the "status" attribute:

* Processing
* Empty
* Completed
* Error
