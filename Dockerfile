FROM ubuntu

RUN apt-get update
RUN apt-get install make python-sphinx python-pip texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended -y
RUN pip install sphinxcontrib-fulltoc sphinxcontrib-httpdomain

WORKDIR /app
VOLUME ["/app"]
CMD ["make", "latexpdf", "html"]
