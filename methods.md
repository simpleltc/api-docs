# API Methods
This document contains descriptions of the API methods available to users of the SimpleLTC API. If you haven't already read the [API Overview](overview.md), you should start there.


## Providers
--------------------------------------------------------------------------------------------------------------------
Methods related to healthcare Providers

### Provider [/Providers/{id}]
Represents a single Provider object which represents a single provider in the SimpleLTC system.

The Provider resource has the following attributes:

- id (int) ... The SimpleLTC-assigned unique identifier for the provider
- name (string) ... The name of the provider
- state (string) ... The two-digit state where the provider is located
- npi (string) ... The National Provider Identifier (NPI) for the provider
- ccn (string) ... The CMS Contract Number (CCN) for the provider (sometimes referred to as the Medicare number)
- mdsFacilityId (string) ... The MDS Facility ID (FAC_ID) for the provider

+ Model (application/json)

        {
            "id":4002,
            "name":"Orange Nursing",
            "state":"TX",
            "npi":"1111111112",
            "ccn":"9987175",
            "mdsFacilityId":"TX1002"
        }

#### Retrieve a single Provider [GET]
+ Returns back the specific Provider resource referenced by the ID

+ Parameters
    + id (int) ... The ID of the provider



### Providers Collection [/Providers]
Collection of Provider resources.

+ Model (application/json)

        [
            {
                "id":4000,
                "name":"Rouster Health",
                "state":"TX",
                "npi":"1111111110",
                "ccn":"9987173",
                "mdsFacilityId":"TX1000"
            },
            {
                "id":4001,
                "name":"Blueth Nursing",
                "state":"TX",
                "npi":"1111111111",
                "ccn":"9987174",
                "mdsFacilityId":"TX1001"
            },
            {
                "id":4002,
                "name":"Orange Nursing",
                "state":"TX",
                "npi":"1111111112",
                "ccn":"9987175",
                "mdsFacilityId":"TX1002"
            }
        ]

#### Retrieve All Providers [GET /Providers]
Retrieves a Providers collection containing all Providers the user has access to.


## MDS Batches & Assessments
--------------------------------------------------------------------------------------------------------------------

SimpleLTC receives groups of MDS batches containing MDS assessments  from providers, transmits them to QIES / CMS, and then automatically retrieves the corresponding final validation reports from the CASPER system. The API allows third-party software to participate in the transmission workflow by acting on behalf of the provider.

In addition, companies and their business associates can use the API to retrieve historical MDS assessment data.

### MDS Batch [/MdsBatches/{id}]
A single MDS batch submission.

The MDS Batch resource has the following attributes:

- id (int) ... The SimpleLTC ID for the specified batch
- provider (Provider) ... The Provider that the batch belongs to
- filename (string) ... The original filename of the batch
- status (string) ... The current status of the batch in the transmission workflow
- webStatus (string) ... The user-friendly version of the status attribute
- statusDate (string) ... The ISO 8601 timestamp of the last update to the batch's status in the transmission workflow.
- uploadDate (string) ... The ISO 8601 timestamp representing when the batch was uploaded to SimpleLTC
- totalRecords (int) ... The total number of MDS assessments in the batch
- cmsBatchID (int) ... The identifier for the MDS batch in the CMS ASAP system.

Possible values for the *status* attribute:

- Waiting for Analysis
- Waiting for Approval
- Rejected by User
- Waiting for Transmit
- Transmitting
- Feedback Received
- Final Received
- On Hold

The attributes *id* and *uploadDate* are populated whenever a batch is first uploaded to SimpleLTC. The *status*, *statusDate*, and *webStatus* attributes are updated as the MDS batch moves through the transmission workflow.

+ Model (application/json)

        {
            "id": 1826984,
            "provider": {
                            "id":4002,
                            "name":"Orange Nursing",
                            "state":"TX",
                            "npi":"1111111112",
                            "ccn":"9987175",
                            "mdsFacilityId":"TX1002"
                    },
            "filename": "123XM16.zip",
            "status": "Final Received",
            "webStatus": "Final Validation Received",
            "statusDate": "2012-10-18T14:00:38.54Z",
            "uploadDate": "2012-10-18T14:00:38.54Z",
            "totalRecords": 21,
            "cmsBatchID": "868281"
        }

#### Retrieve information about a single MDS Batch [GET]
Returns back the specific MDS batch resource referenced by the ID

+ Parameters
    + id (int) ... ID of the MDS Batch resource (required)

#### Download a Single MDS Batch [GET]
Retrieves the raw binary data for a specific batch. This batch will return the binary data of the original file uploaded to SimpleLTC.
This request is differentiated from other get requests by using the proper MIME type.

+ Headers
        Accept: application/octet-stream

+ Parameters
    + id (int) ... ID of the MDS Batch resource (required)

### MDS Batch Collection [/MdsBatches]
Collection of MDS Batch resources

+ Model (application/json)

        [
            {
                "id": 529234,
                "provider": {
                                "id":4002,
                                "name":"Orange Nursing",
                                "state":"TX",
                                "npi":"1111111112",
                                "ccn":"9987175",
                                "mdsFacilityId":"TX1002"
                        },
                "filename": "20110222145414-1.zip",
                "status": "Waiting for Transmit",
                "webStatus": "Pending Transmission",
                "statusDate": "2011-02-23T15:53:21.967Z",
                "uploadDate": "2012-10-03T15:53:21.967Z",
                "totalRecords": 4,
                "cmsBatchID": "868007"
            },
            {
                "id": 529235,
                "provider": {
                                "id":4002,
                                "name":"Orange Nursing",
                                "state":"TX",
                                "npi":"1111111112",
                                "ccn":"9987175",
                                "mdsFacilityId":"TX1002"
                        },
                "filename": "20110221170005-1.zip",
                "status": "Feedback Received",
                "webStatus": "Waiting for Final Validation",
                "statusDate": "2011-02-23T14:42:32.39Z",
                "uploadDate": "2012-10-03T14:42:32.39Z",
                "totalRecords": 5,
                "cmsBatchID": "862794"
            }
        ]

#### Search MDS Batches [GET]
Returns back a MDS Batch collection containing all batches that match the query parameters.

+ Query Parameters
    + provider_id (int) ... Filter to include only batches belonging to the specified provider
    + status_updated_after (datetime) ... Filter to include only batches that have a status date after the specified date
    + status_updated_before (datetime) ... Filter to include only batches that have a status date prior to the specified date
    + status (string) ... Filter to include only batches that have the specified status
    + uploaded_after (datetime) ... Filter to include only batches uploaded after the specified date
    + uploaded_before (datetime) ... Filter to include only batches uploaded before the specified date
    + page (int) ... Indicates which page of the resultset to return (default: 1)

#### Upload a MDS Batch [POST]
Uploads a new MDS batch for transmission to the CMS ASAP system. The MDS batch content should be in the body of the request.

Returns a MDS Batch resource representing the batch

+ Parameters
    + filename (string) ... The filename of the file being uploaded (required)


+ Error Response
        {
            "ErrorCode": 3,
            "Message": "Unrecognized signature for batch: batch.zip: [12]"
        }

+ Error Codes
    + 1 ... The batch is a duplicate and cannot be accepted for upload
    + 2 ... Your account is not authorized to upload for tihs provider. Either the account does not have access or the provider's service was terminated or suspended.
    + 3 ... The batch format is invalid and could not be accepted. Verify the batch is valid and consistent withe the [MDS Submission Specification](http://www.cms.gov/Medicare/Quality-Initiatives-Patient-Assessment-Instruments/NursingHomeQualityInits/NHQIMDS30TechnicalInformation.html)

### MDS Final Validation Report [/MdsFinalValidation/{id}]
The raw final validation report received from CMS for the specified batch

+ Model (text/plain)
                                         CMS Submission Report
                            MDS NH Final Validation Report

        Submission Date/Time:              06/13/2012 13:48:32
        Processing Completion Date/Time:   06/13/2012 13:57:12
        Submission ID:                     54545454
        Submission File Status:            Completed
        State Code:                        TX
        Facility ID:                       555555
        ...

#### Download the Final Validation Report [GET]
Returns the plain text of the Final Validation Report retrieved from CMS for the specified batch

+ Headers
        Accept: text/plain

+ Parameters
    + id (int) ... ID of the MDS Batch resource corresponding to the Final Validation Report (required)

### MDS Assessment [/MdsAssessments/{id}]
A single MDS assessment

The MDS Assessment resource has the following attributes:

- id (int) ... The SimpleLTC ID for the specified MDS Assessment Resource
- batchID (int) ... The SimpleLTC ID for the specified MDS Batch Resource that the assesment belongs to
- residentName (string) ... The name of the resident (MDS: A0500C, A0500A, A0500B)
- dateOfBirth (date) ... The birthdate of the resident (MDS: A0900)
- ssn (string) ... The Social Security Number (SSN) of the resident (MDS: A0600A)
- medicareID (string) ... The Medicare Health Insurance Claim Number (HICN) for the resident (MDS: A0600B)
- medicaidID (string) ... The Medicaid ID number for the resident (MDS: A0700)
- filename (string) ... The filename of the assessment
- assessmentType (string) ... The type of assessment based on the Item Subset Code (ISC)
- versionCode (string) ... The version of the submission specifications that were used to create the assessment (MDS: SPEC_VRSN_CD)
- correction (int) ... The correction number of the assessment (MDS: X0800 if modification or inactivation; otherwise 0)
- targetDate (date) ... The target date of the assessment
- status (string) ... The current status of the assessment in the transmission workflow
- a0200 (string) ... The type of provider (MDS: A0200)
- a0310a (string) ... The OBRA Reason for Assessment (MDS: A0310A)
- a0310b (string) ... The PPS Reason for Assessment (MDS: A0310B)
- a0310c (string) ... The PPS OMRA Reason for Assessment (MDS: A0310C)
- a0310d (string) ... Swing Bed Clinical Change Assessment (MDS: A0310D)
- a0310f (string) ... Entry / discharge reporting (MDS: A0310F)
- isc (string) ... The Item Subset Code (MDS: ITM_SBST_CD)

The attributes *id* and *batchID* are populated whenever a batch is first uploaded to SimpleLTC. The *status* attribute is updated as the MDS batch moves through the transmission workflow.

Possible values for the *status* attribute:

- Accepted
- Rejected
- Waiting

+ Model (application/json)

        {
            "batchID": 528962,
            "id": 2234535,
            "residentName": "MANUEL, DIEGO",
            "dateOfBirth": "1900-01-01",
            "ssn": "000000000",
            "medicaidID": "0000000000",
            "medicareID": "0000000000",
            "filename": "F5_15123_ND_1252011.xml",
            "assessmentType": "Entry Tracking Record",
            "versionCode": "1.00",
            "correction": 0,
            "targetDate": "2011-01-25",
            "status": "Accepted",
            "a0200":"1",
            "a0310a":"99",
            "a0310b":"99",
            "a0310c":"0",
            "a0310d":"^",
            "a0310f":"12",
            "isc":"NT"
        }

#### Retrieve information about a single MDS Assessment [GET]
Returns back the specific MDS Assessment resource referenced by the ID

+ Parameters
    + id (int) ... ID of the MDS Assessment resource

#### Retrieve the XML content of a single MDS Assessment [GET]
Returns back the XML representation of the MDS Assessment referenced by the ID

+ Parameters
    + id (int) ... ID of the MDS Assessment resource


+ Headers
        Accept: application/octet-stream

### MDS Assessment Collection [/MdsAssessments]
Collection of MDS Assessment resources

+ Model (application/json)

        [
            {
                "batchID": 528962,
                "id": 2234535,
                "residentName": "MANUEL, DIEGO",
                "dateOfBirth": "1900-01-01",
                "ssn": "000000000",
                "medicaidID": "0000000000",
                "medicareID": "0000000000",
                "filename": "F5_15123_ND_1252011.xml",
                "assessmentType": "Entry Tracking Record",
                "versionCode": "1.00",
                "correction": 0,
                "targetDate": "2011-01-25",
                "status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT"
            },
            {
                "batchID": 528962,
                "id": 2234536,
                "residentName": "WILSON, JOHN",
                "dateOfBirth": "1900-01-01",
                "ssn": "000000000",
                "medicaidID": "0000000000",
                "medicareID": "0000000000",
                "filename": "F5_15123_NP_232011.xml",
                "assessmentType": "Entry Tracking Record",
                "versionCode": "1.00",
                "correction": 0,
                "targetDate": "2011-02-03",
                "status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT"
            },
            {
                "batchID": 528962,
                "id": 2234538,
                "residentName": "KELLY, SAMUEL",
                "dateOfBirth": "1900-01-01",
                "ssn": "000000000",
                "medicaidID": "0000000000",
                "medicareID": "0000000000",
                "filename": "F5_15636_NC_232011.xml",
                "assessmentType": "Entry Tracking Record",
                "versionCode": "1.00",
                "correction": 0,
                "targetDate": "2011-02-03",
                "status": "Accepted",
                "a0200":"1",
                "a0310a":"99",
                "a0310b":"99",
                "a0310c":"0",
                "a0310d":"^",
                "a0310f":"12",
                "isc":"NT"
            }
        ]

#### Search MDS Assessments [GET]
Returns back a MDS Assessment collection containing all assessments that match the query parameters.

+ Query Parameters
    + provider_id (int) ... Filter to include only assessments belonging to the specified provider
    + batch_id (int) ... filter to include only assessments from a specific MDS Batch
    + status_updated_after (datetime) ... Filter to include only assessments where the associated MDS Batch's status date is after to the specified date
    + status_updated_before (datetime) ... Filter to include only assessments where the associated MDS Batch's status date is prior to the specified date
    + status (string) ... Filter to include only assessments that have the specified status
    + uploaded_after (datetime) ... Filter to include only batches uploaded after the specified date
    + uploaded_before (datetime) ... Filter to include only batches uploaded before the specified date
    + target_date_before (date) ... Filter to include only assessments with a target date prior to the specified date
    + target_date_after (date) ... Filter to include only assessments with a target date after the specified date
    + page (int) ... Indicates which page of the result-set to return (default: 1)

### MDS Assessment Bulk Request [/MdsAssessmentsAsync/{requestId}]
The MDS Assessment Bulk Request allows a API consumer to request that a bulk extract of MDS assessment data be created based on the specified query parameters. Once the system has completed processing the request, the API consumer can use the provided ID to retrieve the MDS extract.

The MDS Assessment Bulk Request resource has the following attributes:

- requestId (string) ... A hash that uniquely identifies the request
- status (string) ... The status of the request

The attributes *id* is populated whenever a request is created and *status* is modified as the request transitions through the workflow.

Possible values for the *status* attribute:

- Processing
- Empty
- Completed
- Error

+ Model (application/json)

        {
            "requestId": "51ddc07773c044275483b67c",
            "status": "Processing"
        }

#### Check the status of a MDS Assessment Bulk Request [GET]
Retrieves the specified MDS Assessment Bulk Request resource

+ Parameters
    + requestId (int) ... ID of the MDS Assessment Bulk Request resource (required)

#### Retrieve the results of a MDS Assessment Bulk request [GET]
Retrieves the ZIP archive containing the XML of the matching MDS assessments from the specified request. If the current request is in "Processing" or "Error" status, this method will return a 404 response.

+ Headers
        Content-type: application/octet-stream

+ Parameters
    + requestId (int) ... ID of the MDS Assessment Bulk Request resource (required)

#### Create a new MDS Assessment Bulk Request [POST]
Submits a new MDS Assessment Bulk Request to the system for processing and returns back the newly created MDS Assessment Bulk Request resource.

+ Headers
        Content-type: application/json

+ Request Attributes -
    The request body should be a JSON object with one or more of the following attributes:
    + provider_id (int) ... Filter to include only assessments belonging to the specified provider (required)
    + batch_id (int) ... filter to include only assessments from a specific MDS Batch
    + status_updated_after (datetime) ... Filter to include only assessments where the associated MDS Batch's status date is after to the specified date
    + status_updated_before (datetime) ... Filter to include only assessments where the associated MDS Batch's status date is prior to the specified date
    + status (string) ... Filter to include only assessments that have the specified status
    + uploaded_after (datetime) ... Filter to include only batches uploaded after the specified date
    + uploaded_before (datetime) ... Filter to include only batches uploaded before the specified date
    + target_date_before (date) ... Filter to include only assessments with a target date prior to the specified date
    + target_date_after (date) ... Filter to include only assessments with a target date after the specified date


## PBJ Batches and Finals (beta)
--------------------------------------------------------------------------------------------------------------------
SimpleLTC receives zipped PBJ batches containing xml files, transmits them to QIES / CMS, and then automatically retrieves the corresponding final validation reports from the CASPER system. The API allows third-party software to participate in the transmission workflow by acting on behalf of the provider.

In addition, companies and their business associates can use the API to retrieve historical PBJ data.

This API is currently in beta. There might be some changes to fields presented in the future, but the method endpoints should stay the same.

### PBJ Batch [/PBJBatches/{id}]
A single PBJ batch submission

The PBJ Batch resource has the following attributes:

- id (int) ... The SimpleLTC ID for the specified batch
- batch_status_id (int) ... A status ID which corresponds to where the batch is in the workflow
- submission_id (int) ... The CMS assigned ID for the submission
- upload_date (datetime) ... The date the batch was uploaded to SimpleLTC, in UTC
- transmit_date (datetime) ... The date the batch was uploaded to CMS, in UTC
- upload_records (int) ... The number of records (xml files) in the PBJ batch
- author_id (int) ... The ID of the user that uploaded the batch to CMS
- filename (string) ... The filename of the batch uploaded to SimpleLTC
- final_date (datetime) ... The date the final was generated by CMS
- processed (int) ... The number of PBJ records processed by CMS as listed in the final
- accepted (int) ... The number of PBJ records accepted by CMS as listed in the final
- rejected (int) ... The number of PBJ records rejected by CMS as listed in the final
- no_authority (int) ... The number of PBJ records marked as No Authority as listed in the final
- fiscal_quarter (int) ... The fiscal year for the batch, as an int from 1 to 4
- fiscal_year (int) ... the fiscal year for the batch, in YYYY notation.


+ Model (application/json)
```
{
    "id": 1234,
    "batch_status_id": 3,
    "submission_id": 0,
    "upload_date": "2016-06-29T23:52:48.793333Z",
    "transmit_date": "0001-01-01T06:00:00Z",
    "upload_records": 1,
    "author_id": 37818,
    "filename": "example.zip",
    "final_date": "0001-01-01T06:00:00Z",
    "processed": 0,
    "accepted": 0,
    "rejected": 0,
    "no_authority": 0,
    "fiscal_quarter": 1,
    "fiscal_year": 2017
}
```

#### Retrieve information about a single PBJ Batch [GET]
Returns back the specific PBJ batch resource referenced by the ID
+ Parameters
    + id (int) ... ID of the PBJ Batch resource

#### Download a Single PBJ Batch [GET]
Retrieves the raw binary data for a specific batch. This batch will return the binary data of the original ZIP file uploaded to SimpleLTC. This request is differentiated from the other GET requests by using the proper MIME type.

+ Headers
        Content-type: application/octet-stream

+ Parameters
    + id (int) ... ID of the PBJ Batch resource (required)

#### Upload a PBJ Batch [POST]
Uploads a new PBJ batch for transmission to the CMS ASAP system. The PBJ batch content should be in the body of the request. The method does some basic checks of the format of all files inside the batch to ensure the files in the ZIP are valid PBJ files and will return with a list of errors, if any.

Returns a JSON object with the id of the batch.

+ Parameters
    + filename (string) ... The filename of the file being uploaded (required)


+ Error Response
```
[
    {
        "filename": "example1.xml",
        "message": "This is an example error message"
    },
    {
        "filename": "example2.xml",
        "message": "This is a different error message for another file."
    }
]
```

### PBJ Final [/PBJFinals/{id}]
PBJ Finals are available in a parsed JSON format or as the raw PDF. They are queried by using the PBJ Batch ID.

The PBJ Final resource has the following attributes:

- submission_id (int) ... The CMS issued Submission ID
- pbjs (JSON List) ... A list of PBJ files with specific information on each file.
- processed (int) ... The number of PBJ files that were processed by CMS
- accepted (int) ... The number of accepted PBJ files in the batch
- rejected (int) ... The number of rejected PBJ files in the batch
- no_authority (int) ... The number of files that CMS determined the user had no authority to uploaded
- messages (int) ... The count of messages issued from CMS
- final_date (datetime) ... The datetime of when the final was issued.

+ Model (application/json)
```
{
    "submission_id": 12345,
    "pbjs":[
        {
            "id": 9,
            "cms_pbj_id": 8675309,
            "status": "Accepted",
            "messages":[
                {
                    "item": null,
                    "item_value": null,
                    "message": "This is an example message, not a real error message.",
                    "message_number": -4009
                }
            ],
            "filename": "example-census.xml"
        }
    ],
    "processed": 1,
    "accepted": 1,
    "rejected": 0,
    "no_authority": 0,
    "messages": 1,
    "final_date": "2016-06-30T05:33:27Z"
}
```

#### Retrieve information about a single PBJ Final [GET]
Returns back the specific PBJ final resource referenced by the Batch ID
+ Parameters
    + id (int) ... ID of the PBJ Batch resource

#### Download a Single PBJ Final [GET]
Retrieves the raw binary data for a specific final. This batch will return the binary data of the original PDF file issued by CMS and retrieved SimpleLTC. This request is differentiated from the other GET requests by using the proper MIME type.

+ Headers
        Content-type: application/octet-stream

+ Parameters
    + id (int) ... ID of the PBJ Batch resource (required)


## User Provisioning
--------------------------------------------------------------------------------------------------------------------

### User [/Users/{id}]
Represents a single User Account in the SimpleLTC system

The User resource has the following attributes:

- id (int) ... The unique user ID number for the User account
- username (string) ... The user's username (required)
- first_name (string) ... The user's first name (required)
- last_name (string) ... The user's last name (required)
- email (string) ... The user's e-mail address (required)
- title (string) ... The user's job title
- external\_id (string) ... The user's external ID, if any
- expiration\_dt (date) ... After this date, the user's account will be disabled
- created\_dt (datetime) ... The timestamp indicating when the account was created
- password (string) ... The user's password
- roles (AssignedRole[]) ... A Role collection containing the user's assigned roles
- enabled (bool) ... True if the account is enabled, False if it is disabled.

The *id* and *created_dt* are automatically populated when the user account is created.  
The *password* attribute is write-only and can only be used during initial account creation or when posting modifications.  
The *username* attribute is read-only and can only be set during initial account creation.  

+ Model (application/json)

        {
            "id": 123634,
            "username": "john.doe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "john.doe@happyhills.com",
            "title": "Facility Administrator",
            "external_id": "A1125543",
            "expiration_dt": null,
            "created_dt": "2014-11-19T18:58:02Z",
            "enabled": true,
            "roles": [
                    { "role_id": 7, "subject_id": "P4002" },
                    { "role_id": 10, "subject_id": "A" },
                ]
        }

#### Retrieve information about a specific user account [GET]
Retrieves the User resource specified by the ID

+ Parameters
    + id (int) ... ID of the User resource

#### Modify a specific user account [PUT]
Modifies the user account specified by the ID using the POST'd User resource

+ Parameters
    + id (int) ... ID of the User resource



### Assigned Role
Represents a role assigned to a user in the SimpleLTC system

The SimpleLTC system uses role-based access control to assign rights to users. Roles are assigned to users by combining the role ID with the ID of the subject. The subject can be either a specific provider or all providers within the company.

The AssignedRole entity has the following attributes:

- role_id (int) ... The ID of the Role to be assigned
- subject_id (string) ... The subject of the privileges (provider, group, all)

+ Valid values for subject ID:
    + 'A' ... All providers in the company
    + 'P####' ... A specific provider where #### is the Provider resource's id.


+ Valid values for role_id:

    + 7 ... MDS Staff
    + 8	- Business Office Staff
    + 9	- Management
    + 10 ... Auditor
    + 11 ... Account Administrator*
    + 16 ... TX ... MESAV Note Exclusions
    + 22 ... TX ... PASRR Submitter
    + 23 ... Analysis Rule Manager*
    + 24 ... User Manager*

    Some roles can only be assigned for all providers in the company. These roles have been denoted with a `*`

The example role below would grant the user "MDS Staff" access to the Provider with id = 4002.

+ Model (application/json)

        {
            "role_id": 7,
            "subject_id": "P4002"
        }

### User Collection [/Users]
Collection of User resources

+ Model (application/json)

        [
            {
                "id": 123634,
                "username": "john.doe",
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@happyhills.com",
                "title": "Facility Administrator",
                "external_id": "A112553",
                "expiration_dt": null,
                "created_dt": "2014-11-19T18:58:02Z",
                "enabled": true,
                "roles": [
                        { "role_id": 7, "subject_id": "P4002" },
                        { "role_id": 10, "subject_id": "A" },
                    ]
            },
            {
                "id": 123635,
                "username": "jane.smith",
                "first_name": "Jane",
                "last_name": "Smith",
                "email": "jane.smith@happyhills.com",
                "external_id": "A112554",
                "title": "Nurse",
                "expiration_dt": null,
                "created_dt": "2014-11-18T18:58:02Z",
                "enabled": true,
                "roles": [
                        { "role_id": 7, "subject_id": "P4002" }
                    ]
            }
        ]

#### Retrieve information about users [GET]
Returns all assigned users,

+ Query Parameters
    + username (string) ... Filter to include only users matching that username
    + email (string) ... Filter to include only users matching that email
    + external\_id ... Filter to include only users matching that external id
    + enabled (boolean) ... Filter to include only active or inactive users

#### Create a new user [POST]
Creates a new user account and, if successful, returns back the User resource representing that account

#### Upsert a new user [PUT]
Creates or updates a new user and, if successful, returns back the User resource representing that account.


## Service Coordination Notifications (Managed Care Organizations Only)
--------------------------------------------------------------------------------------------------------------------
Methods related to service coordination notifications for managed care organizations

### SCN [/SCN/{id}/]
Retrieve a single SCN with the given id (Note: This request will only succeed if your API account is associated with
the SCN's target MCO)

#### Retrive a single SCN [GET]
Retrieves a single SCN with the provided id

 + Parameters
     + id (int) ... A unique id for SimpleLTC's internal representation of an SCN

 + Returns SCN - JSON with a single SCN object

### SCN Batch [/SCN/batch/]
Retrieve multiple SCNs for the MCO tied to your API account

#### Retrieve multiple SCNs [POST]
Retrieves all of the SCNs with the provided list of ids

 + Headers
     + Content-Type: application/json

 + Request Attributes - The request body should be a JSON string with the following attributes:
     + int[] ids ... A list of unique ids for SimpleLTC's internal representation of an SCN

     + Example body: "[203, 204, 205, 210]"

 + Returns SCN[] - JSON with an array of SCN objects

### SCN's Since [/SCN/?since={since_date}]
Retrieve all of the submitted SCN's since a given date for the MCO tied to your API account

#### Retrieve SCN's since [GET]
Retrieves all of the submitted SCN's since a given date for the MCO tied to your API account

+ Parameters
    + since (string) ... An ISO 8601 formatted date that indicates which SCN's will be returned

+ Returns SCN[] - JSON with and array of SCN objects

The SCN object has the following attributes:

- UID (int) ... The SimpleLTC-assigned unique identifier for the SCN form
- Creator (string) ... The name of the user who first created the form
- Submitter (string) ... The name of the user who submitted the form
- ConditionChange (string) ... The value of the Condition Change field from the SCN form
- AdmissionDate (string) ... The value of the Admission Date field from the SCN form (ISO 8601)
- DischargeDate (string) ... The value of the Discharge Date field from the SCN form (ISO 8601)
- DischargeTo (string) ... The value of the selected Discharge To option from the SCN form
- DischargeToDescription (string) ... The value of the additional Discharge To information from the SCN form
- PayorChange (string) ... The valud of the selected Payor Change option from the SCN form
- PayorChangeDate (string) ... The value of the Payor Change Date field from the SCN form (ISO 8601)
- ERDate (string) ... The value of the Emergency Room Visit Date field from the SCN form (ISO 8601)
- ERReturnDate (string) ... The value of the Emergency Room Return Date field from the SCN form (ISO 8601)
- ERFacility (string) ... The value of the ER Facility field from the SCN form
- ERDescription (string) ... The value of the ER Reason field from the SCN form
- FacilityRep (string) ... The value of the Facility Representative field from the SCN form
- Submitted (string) ... The date and time that the SCN form was submitted (ISO 8601)
- DADSReport (string) ... The value of the DADS Report field from the SCN form
- DADSReportDate (string) ... The value of the DADS Report Date field from the SCN form (ISO 8601)
- AuthorizationRequest (string) ... The value of the Authorization Request field from the SCN form
- Facility Name (string) ... The name of the facility from which the SCN was submitted
- Phone (string) ... The phone number of the facility from which the SCN was submitted
- VendorNumber (string) ... The vendor number for the facility from which the SCN was submitted
- LastName (string) ... The last name of the resident for who the SCN was submitted
- FirstName (string) ... The first name of the resident for who the SCN was submitted
- MiddleInitial (string) ... The middle initial of the resident for who the SCN was submitted
- Suffix (string) ... The suffix of the resident for who the SCN was submitted
- DateOfBirth (string) ... The date of birth of the resident for who the SCN was submitted (ISO 8601)
- MedicaidID (string) ... The Medicaid ID of the resident for who the SCN was submitted

SCN Resource

+ Model (application/json)

        {
            "UID": 15,
            "Creator": "Sherri Harris",
            "Submitter": "Sherri Harris",
            "ConditionChange": "Some value",
            "AdmissionDate": "2015-04-21T00:00:00Z",
            "DischargeDate": "2015-04-22T00:00:00Z",
            "DischargeTo": "Hospital",
            "DischargeToDescription": "Hospital Name",
            "PayorChange": "Skilled Bed",
            "PayorChangeDate": "2015-04-21T00:00:00Z",
            "ERDate": "2015-04-21T13:00:00Z",
            "ERReturnDate": "2015-04-20T13:00:00Z",
            "ERFacility": "Facility Name",
            "ERDescpription": null,
            "FacilityRep": "Sherri Harris",
            "Submitted": "2015-04-20T11:15:22Z",
            "DADSReport": "Some  value",
            "DADSReportDate": "2015-04-21T00:00:00Z",
            "AuthorizationRequest": "Some value",
            "FacilityName": "Crowley Nursing & Rehab.",
            "Phone": "123-456-7890        ",
            "VendorNumber": "5421",
            "LastName": "Vandelay",
            "FirstName": "Art",
            "MiddleInitial": "L",
            "Suffix": null,
            "DateOfBirth": "1951-10-23T00:00:00Z",
            "MedicaidID": "123456789"
        }
