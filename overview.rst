Overview
=========================================

This documentation contains an explanation of the SimpleLTC API and its public
methods. It is meant as a guide for SimpleLTC customers and API partners to use
when building solutions that integrate with SimpleLTC applications.

API support is provided by contacting api-support@simpleltc.com.
		

