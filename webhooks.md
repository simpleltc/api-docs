# Web Hooks
The SimpleLTC API supports scenarios where notification can be provided to third-party API partners through JSON web hooks. If you haven't already read the [API Overview](overview.md), you should start there.

API partners wishing to take advantage of web hooks must provide a web hook endpoint URL to SimpleLTC. This URL is where the JSON web hook data will be posted when events occur. From that endpoint, third-party API partners can take whatever action they decide is necessary based on the content of the JSON object. Currently, in order to configure a web hook endpoint, third-party API partners must create a support ticket with SimpleLTC Support by e-mailing api-support@simpleltc.com indicating which webhooks you would like to receive.

Web hook URLs must be secured using SSL / HTTPS since they may contain sensitive information.

## JSON Web Signature (JWS)
[JSON Web Signature (JWS)](http://tools.ietf.org/html/draft-jones-json-web-signature) "represents content secured with digital signatures or Message Authentication Codes (MACs) using JavaScript Object Notation (JSON)".
Essentially, JWS allows us to digitally sign an arbitrary piece of content (the payload) to verify authenticity (it came from the intended sender) and integrity (nobody has modified it while in transit). The header contains information about the encryption algorithm used for the digital signature. The payload contains the message that needs to have its authenticity and integrity assured. The signature is used to verify the payload has not been modified.

JSON web objects consist of three parts: a header, the payload, and the signature. All three parts are strings that are Base64 encoded using the URL- and filename-safe character set defined in Section 5 of RFC 4648 without padding characters ('='). The three parts are concatenated by the "." character.

For SimpleLTC web hooks, all requests are signed with SimpleLTC's private key using RSASSA-PKCS-v1_5 with SHA-256 hash (RS256) as stated by the parameters in the header of each web hook request. The signature for all SimpleLTC web hooks can and should be verified by using the RSA public key referenced by the "kid" and "jku" parameters of the header. A copy of the referenced RSA public key is available in JSON Web Key (JWK) format at the URL referenced by the "jku" parameter. A version of the RSA public key is published in PEM format at https://api.simpleltc.com/keys/{kid}.pub.pem

While it is possible to decode and use the payload of the request without verifying the signature, all API partners are STRONGLY ENCOURAGED to verify the digital signature against SimpleLTC's public key before taking action based on a web hook. If the signature is not verified, there will be no assurance that the token originated from SimpleLTC and has not been changed in-transit. If the signature fails verification, the web hook should be discarded.

A number of third-party open-source libraries exist for implementing JWS. It is reccomnended that implementers take advantage of one of the existing implementations.

For more detailed information about JWS, please review the IETF specification for [JSON Web Signature](http://tools.ietf.org/html/draft-jones-json-web-signature)

An example web hook protected by JWS is included below for your reference (please note, line breaks are added for readability):

    eyJhbGciOiAiUlMyNTYiLCAia2lkIjogInNsdGMtd2ViaG9vay0yMDE0MDEwMS0wMSIsICJqa3UiOiAiaHR0cHM6Ly9hcGkuc2ltcGxlbHRjLmNvbS9rZXlzL2N1cnJlbnRfa2V5c2V0Lmpzb24ifQ
    .
    eyJ0eXBlIjogMSwgImNvbXBhbnlfbmFtZSI6ICJTdW5ueSBIaWxscyBIb21lcywgSW5jLiIsICJhdXRoX3VzZXJuYW1lIjogImFwaV8zMjNmemEzMkBjb21wYW55Y29kZSIsICJhdXRoX3Bhc3N3b3JkIjogIjMyOGRhc3VkYTgjOXM_IyEiLCAiaW5pdGlhdGVkX2J5IjogImpvaG5kb2VAY29tcGFueWNvZGUiLCAiaWF0IjogMTM5MTU1NzI5NX0
    .
    DnERwd_iUo2EIf4vDkynfjlRyMreQLBeW6E3KlZv3zvUYkmLN37pgvMFRmR6yKuAIAohNgtNFEzyrZAzVHf0cWxGo4XadBtXJgXGWmw8daTyJCbkXuLyj9rooSaWi2kxRnlK4olC8kKu3Fw9lJ7_2pMxkTwo8kojxLlbenqKmq_ZztWxM2Icgiy31Kd5n4nBBDR2h4u959m5RlQETxb30RLMEVooUP6hVn0dPYCsMPZFmbhEagZ0Bo87zKEzzFTtpQbv0w8dQo6RRvgDzQWOm5C8MkpPCm3z__PNpvRrExX8z78uaYM-2eUHqTkjO6xym9ph_4zGBZrIDArXTURlgA

Web hook protected by JWS example without line breaks (included for testing purposes):

    eyJhbGciOiAiUlMyNTYiLCAia2lkIjogInNsdGMtd2ViaG9vay0yMDE0MDEwMS0wMSIsICJqa3UiOiAiaHR0cHM6Ly9hcGkuc2ltcGxlbHRjLmNvbS9rZXlzL2N1cnJlbnRfa2V5c2V0Lmpzb24ifQ.eyJ0eXBlIjogMSwgImNvbXBhbnlfbmFtZSI6ICJTdW5ueSBIaWxscyBIb21lcywgSW5jLiIsICJhdXRoX3VzZXJuYW1lIjogImFwaV8zMjNmemEzMkBjb21wYW55Y29kZSIsICJhdXRoX3Bhc3N3b3JkIjogIjMyOGRhc3VkYTgjOXM_IyEiLCAiaW5pdGlhdGVkX2J5IjogImpvaG5kb2VAY29tcGFueWNvZGUiLCAiaWF0IjogMTM5MTU1NzI5NX0.DnERwd_iUo2EIf4vDkynfjlRyMreQLBeW6E3KlZv3zvUYkmLN37pgvMFRmR6yKuAIAohNgtNFEzyrZAzVHf0cWxGo4XadBtXJgXGWmw8daTyJCbkXuLyj9rooSaWi2kxRnlK4olC8kKu3Fw9lJ7_2pMxkTwo8kojxLlbenqKmq_ZztWxM2Icgiy31Kd5n4nBBDR2h4u959m5RlQETxb30RLMEVooUP6hVn0dPYCsMPZFmbhEagZ0Bo87zKEzzFTtpQbv0w8dQo6RRvgDzQWOm5C8MkpPCm3z__PNpvRrExX8z78uaYM-2eUHqTkjO6xym9ph_4zGBZrIDArXTURlgA

## Initial Linking

When a new API account is established for use by a third-party API partner for a specific company, the following web-hook format will be POSTed to the configured web hook endpoint. Once this message is received, the API partner should typically execute a request against the /Providers API to retrieve a list of providers that the account has access to.

+ Example Webhook Payload

        {
            "type": 1,
            "company_name": "Sunny Hills Homes, Inc.",
            "auth_password": "328dasuda8#9s?#!",
            "auth_username": "api_323fza32@companycode",
            "initiated_by": "johndoe@companycode",
            "iat": 1391557295,
        }

+ Properties:
    + type (int): Indicates the type of web hook. In this case, a value of 1 indicates an initial linking message.
    + company_name (string): The name of the company granting access as shown in the SimpleLTC system.
    + auth_username (string): The username that should be used for API requests for this company
    + auth_password (string): The password that should be used for API requests for this company
    + initiated_by (string): The username that authorized the partner linking
    * iat (int): A timestamp indicating when the message was generated (to help prevent replays)


## Batch has Final Received Status

When a batch that has been submitted to CMS has reached Final Received Status, the following web hook format will be POSTed to the configured web hook endpoint.

+ Example Webhook Payload

        {
           "type": 2,
            "batch_id": "1826984",
            "provider_uid": "4002",
            "iat": 1391557295,
         }

+ Properties:
    + type (int): Indicates the type of web hook. In this case, a value of 2 indicates a batch marked Final Received by CMS.
    + batch_id (int): The ID of the batch
    + provider_id (int): The ID of the provider
    * iat (int): A timestamp indicating when the message was generated (to help prevent replays)


## Service Coordination Notification Received (Managed Care Organizations Only)

When a service coordination notification has been submitted by a provider, the following web hook format will be POSTed to the configured web hook endpoint.

+ Example Webhook Payload

        {
           "type": 3,
            "mco_id": "1",
            "doc_id": "3432",
            "iat": 1391557295,
         }

+ Properties:
    + type (int): Indicates the type of web hook. In this case, a value of 3 indicates an SCN submission.
    + mco_id (int): SimpleLTC's iternal UID for a managed care organization. Used to query the API.
    + doc_id (int): SimpleLTC's internal UID for the SCN form that was submitted. Used to query the API.
    + iat (int): A timestamp indicating when the message was generated (to help prevent replays)
