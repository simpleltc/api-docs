Date/Time Values
***************************************

**Date/Time Format**

All datetime values provided in API parameters and responses are expected to be in ISO8601 format.

**Local Time**

"Local Time" is defined as the current time zone of Dallas, TX, USA.

**Request Parameters**

All datetime request parameters must be in ISO 8601 format. Time zone specifiers will be respected in parameters.

*Examples*

* "2013-01-01" or "2013-01-01T00:00" will be interpreted as January 1st, 2013 Midnight Local Time
* "2013-01-01T10:00Z" will be interpreted as January 1st, 2013 10:00 AM UTC
* "2013-01-01T10:00Z-05:00" will be interpreted as January 1st, 2013 03:00 PM UTC

**Response Parameters**

All datetime values in XML / JSON responses will be returned in ISO 8601 UTC format.
Date-only values will be returned in 'yyyy-mm-dd' format with no time zone specifier (since they, by definition, contain only the date value)

