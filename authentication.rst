Authentication
======================================

In order to make calls to the SimpleLTC API, two authentication headers must be submitted with each and every API request. For simplicity,
these parameters have been left out of the documentation of all API methods and the sample requests and responses. Their presence is implied in all API calls.

	* HTTP Authorization
		The client must send a username, password, and company code for a SimpleLTC API account using the format specified for the Basic authentication scheme as specified
		in RFC 2617 "Basic Authentication Scheme". More information about SimpleLTC API accounts can be found in the section titled "Authorization".
		From RFC 2617: "To receive authorization, the client sends the information as a string formatted as follows base64({username}@{companyCode}:{password}).

		Below is a sample of a proper authorization header for the username "username", company code "northwind", and password "password1".

		.. sourcecode:: http

			Authorization: Basic dXNlckBub3J0aHdpbmQ6cGFzc3dvcmQx

	* Developer API Key
		Each software vendor must have a unique developer API key assigned by SimpleLTC in order to make API requests. The API key
		should be sent using the "SLTC-Api-Key" header as shown below.

		.. sourcecode:: http
			
			SLTC-Api-Key: 11111111-2222-3333-4444-abcdefabcdef
		

