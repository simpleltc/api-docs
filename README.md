This repository contains information about the SimpleLTC API for third-party and customer integrations.

If you're interested in working with the API, you should start with the [API Overview](overview.md)

For API Support, please contact [api-support@simpleltc.com](mailto:api-support@simpleltc.com) or contact the SimpleLTC Support Team at 469-916-2803.
