Error Messages
***************************************

**Error Format**

Most API error messages will be returned in the following JSON format:

.. sourcecode:: json

   {
       "Message":"The request could not be processed because an unknown error occured."
   }

Some API methods return custom errors which may include additional information. Please see method specific documentation for more information.


**Maintenance**

The API undergoes periodic maintenance periods for upgrades and improvements. These maintenance periods will be announced to all of the registered contacts for each API partner.

	**Example Response**

	.. sourcecode:: http

		HTTP/1.1 503 Service Unavailable
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Content-Language: en-US
		Expires: -1
		Date: Thu, 13 Dec 2012 15:30:13 GMT
		Content-Length: 83

		{"Message":"The system is currently down for maintenance. Please try again later."}



