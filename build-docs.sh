#!/bin/bash
run_cmd="docker run --rm -v `pwd`:/app docker.sltc.local:5000/api-docs"

if docker run --rm -v `pwd`:/app docker.sltc.local:5000/api-docs; then
    echo ''
    echo 'Run complete.'
    echo 'PDF output: _build/latex'
    echo 'HTML output: _build/html'
else
    echo 'Error occurred while running the container.'
fi


