Providers
***************************************

List Providers
======================================

.. http:get:: /Providers

   Lists all providers that the current user can access. The returned data will return the state abbreviation and any facility identifiers that SimpleLTC has on file incuding the MDS facility ID, Medicare CCN, and NPI.

   **Example Request**

   .. sourcecode:: http
      
		GET /Providers HTTP/1.1
		Accept: application/json

   **Example Response**

   .. sourcecode:: http

		HTTP/1.1 200 OK
		Cache-Control: no-cache
		Pragma: no-cache
		Content-Type: application/json; charset=utf-8
		Expires: -1
		Date: Thu, 25 Oct 2012 17:17:51 GMT
		Content-Length: 416

		[
                    {
                        "id":4000,
                        "name":"Rouster Health",
                        "state":"TX",
                        "npi":"1111111110",
                        "ccn":"9987173",
                        "mdsFacilityId":"TX1000"
                    },
                    {
                        "id":4001,
                        "name":"Blueth Nursing",
                        "state":"TX",
                        "npi":"1111111111",
                        "ccn":"9987174",
                        "mdsFacilityId":"TX1001"
                    },
                    {
                        "id":4002,
                        "name":"Orange Nursing",
                        "state":"TX",
                        "npi":"1111111112",
                        "ccn":"9987175",
                        "mdsFacilityId":"TX1002"
                    }
		]

